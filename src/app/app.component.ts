import { Component } from '@angular/core';
import {products} from "./products";

var Ajv = require('ajv');
var ajv=new Ajv();

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css','../../node_modules/@progress/kendo-theme-default/dist/all.css']
})
export class AppComponent {
  title = 'app';
  validateRes = '';
  validateErrorText = '';
  text = "";
  textJson = {};

  private readSingleFile(e:any) {
    var file = e.target.files[0];
    console.log(file);
    if (!file) {
      // if (!file.type.match('text')) {
      return;
    }
    var reader = new FileReader();
    reader.onload = file => {
      var contents = file.target;
      this.text = contents['result'];
      this.textJson = JSON.parse(contents['result']);
    };
    reader.readAsText(file);
    // console.log(reader.readAsText(file))
  }

  jvf() {
    var validate = ajv.compile(this.textJson[0]);
    var valid = validate(this.textJson[1]);
    if (!valid) console.log(validate.errors);
    this.validateRes = valid;
    this.validateErrorText = JSON.stringify(validate.errors);
  }
  private gridData:any[] = products;
}
